/**
 * Classe Automato Usada para Representar um autômato.
 */
package automatos;

import java.util.ArrayList;

/**
 *
 * @author Hugaleno e Stéphanie
 */
public class Automato {

    private String aux;
    private final String[] ro;
    private final String estadoInicial;
    private final String[] estados;
    private final String[] alfabeto;
    private final String[] estadosMarcados;
    private final String[] f;
    private final ArrayList<Estado> estadosIniciados = new ArrayList<>();
    private Estado estadoAtual;

    /**
     * Construtor Pega um Array de String contendo o autômato lido de um arquivo
     * e trata, atribuindo a seus respectivos atributos.
     *
     * @param atributosAutomato String ArrayList
     */
    public Automato(ArrayList<String> atributosAutomato) {
        estados = atributosAutomato.get(0).split(",");
        alfabeto = atributosAutomato.get(1).split(",");
        aux = atributosAutomato.get(2).replaceAll("\\[", "");
        aux = aux.replaceAll("\\(", "");
        aux = aux.replaceAll("\\)", "");
        this.f = aux.split("],");
        this.ro = atributosAutomato.get(3).split(",");
        this.estadoInicial = atributosAutomato.get(4).trim();
        this.estadosMarcados = atributosAutomato.get(5).split(",");
        //Trata as entradas para criar os objetos da classe estado com os dados corretos.
        for (int i = 0; i < estados.length; i++) {
            estadosIniciados.add(new Estado(estados[i]));
        }

        for (String funcao : f) {
            avaliaFuncao(funcao);
        }

        for (Estado estado : estadosIniciados) {
            if (estado.getNomeDoEstado().equals(estadoInicial)) {
                estado.setEstadoInicial(true);
                estadoAtual = estado;
            }
            for (String em : estadosMarcados) {
                if (estado.getNomeDoEstado().equals(em.trim())) {
                    estado.setEstadoMarcado(true);
                    estado.marcaEstadosQMeConhecem();
                }
            }

        }

    }

    /**
     * Avalia a função de transições do autômato e relaciona todos os estados do
     * autômato a seus respectivos eventos e estados destino.
     *
     * @param funcao String que indica um evento
     * @return void
     */
    private void avaliaFuncao(String funcao) {
        String auxiliar[];
        auxiliar = funcao.split(",");
        for (Estado estado : estadosIniciados) {
            if (estado.getNomeDoEstado().equals(auxiliar[0].trim())) {
                estado.addTransicao(auxiliar[1], auxiliar[2]);
                if (!estado.getNomeDoEstado().equals(auxiliar[2])) {
                    getEstadoPeloNome(auxiliar[2]).addEstadosQMeConhecem(estado);
                }
                break;
            }
        }
    }

    /**
     * Retorna se o estado destino do evento é marcado ou não.
     *
     * @param str String que indica um evento
     * @return boolean
     */
    public boolean estaMarcado(String str) {
        for (Estado ei : estadosIniciados) {
            String auxiliar;
            if ((auxiliar = ei.temEvento(str)) != null) {
                for (String em : estadosMarcados) {
                    if (auxiliar.equals(em)) {
                        return true;
                    }
                }

            }
        }
        return false;
    }

    /**
     * Retorna se um evento faz parte dos eventos ativos
     *
     * @param get String que indica um evento
     * @return boolean
     */
    public boolean verificaRo(String get) {
        for (String ro1 : ro) {
            if (ro1.trim().equals(get)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Recebe um nome e retorna o estado associado ao nome.
     *
     * @param std String que indica o nome de um estado.
     * @return Estado
     */
    public Estado getEstadoPeloNome(String std) {
        for (Estado estado : estadosIniciados) {
            if (std.equals(estado.getNomeDoEstado())) {
                return estado;
            }
        }
        return null;
    }

    /**
     * Retorna se uma palavra é ou não reconhecida pelo autômato.
     *
     * @param evento String ArrayList que indica a palavra.
     * @return String
     */
    public String reconhecePalavra(ArrayList<String> evento) {
        String auxiliar;

        for (int i = 0; i < evento.size(); i++) {
            auxiliar = estadoAtual.temEvento(evento.get(i));
            if (auxiliar == null) {
                estadoAtual = getEstadoPeloNome(estadoInicial);
                return "Palavra não reconhecida!";
            }
            if (i == evento.size() - 1) {
                if (estaMarcado(evento.get(i))) {
                    estadoAtual = getEstadoPeloNome(estadoInicial);
                    return "Palavra reconhecida";
                }
            } else {

                estadoAtual = getEstadoPeloNome(auxiliar);

            }

        }
        estadoAtual = getEstadoPeloNome(estadoInicial);
        return "Palavra não reconhecida!";

    }

    /**
     * Retorna os estados Coacessiveis do autômato
     *
     * @return estadosCoacessiveis, ArrayList de Estados.
     */
    public ArrayList<Estado> parteCoacessivel() {
        ArrayList<Estado> estadosCoacessiveis = new ArrayList<>();
        for (Estado std : estadosIniciados) {
            if (std.isCoacessivel()) {
                estadosCoacessiveis.add(std.clone());
            }
        }

        return estadosCoacessiveis;
    }

    /**
     * Retorna os estados Acessiveis do autômato
     *
     * @return acessivel, ArrayList de Estados.
     */
    public ArrayList<Estado> parteAcessivel() {

        ArrayList<Estado> auxiliar = estadosIniciados;
        ArrayList<Estado> acessivel = new ArrayList<>();
        for (String std : estados) {
            estadoAtual = getEstadoPeloNome(std);
            if (estadoAtual.isEstadoInicial()) {
                estadoAtual.setAcessado(true);
            } else {
                for (Estado state : auxiliar) {
                    if (!estadoAtual.equals(state)) {
                        if (state.getTransicoes().containsValue(estadoAtual.getNomeDoEstado())) {
                            estadoAtual.setAcessado(true);

                        }
                    }
                }

            }
        }
        for (Estado acs : auxiliar) {
            if (acs.getAcessado()) {
                acessivel.add(acs);
            }

        }
        return acessivel;
    }

}
