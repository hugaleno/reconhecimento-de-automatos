/*
 * Classe Estado
 * Usada para representar um estado do autômato.
 */
package automatos;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Hugaleno e Stéphanie
 */
public class Estado {

    private String nomeDoEstado;
    private boolean estadoInicial = false;
    private boolean acessado = false;
    private boolean coAcessivel = false;
    private boolean estadoMarcado = false;
    private boolean jaAnalisado = false;
    private ArrayList<String> eventos;
    private ArrayList<Estado> estadosQMeConhecem;
    private HashMap<String, String> transicoes;

    public Estado() {
        this.estadosQMeConhecem = new ArrayList<>();
        this.eventos = new ArrayList<>();
        this.transicoes = new HashMap<>();
    }
    
    public Estado(String nomeDoEstado) {
        this.estadosQMeConhecem = new ArrayList<>();
        this.eventos = new ArrayList<>();
        this.transicoes = new HashMap<>();

        this.nomeDoEstado = nomeDoEstado;

    }

    public Estado(String nomeDoEstado, boolean estadoInicial, boolean estadoMarcado) {
        this.estadosQMeConhecem = new ArrayList<>();
        this.eventos = new ArrayList<>();
        this.transicoes = new HashMap<>();
        this.nomeDoEstado = nomeDoEstado;
        this.estadoInicial = estadoInicial;
        this.estadoMarcado = estadoMarcado;

    }

    /**
     *  Retorna todos os outros estados que levam a um estado.
     * @return estadosQMeConhecem, ArrayList de Estados.
     */
    public ArrayList<Estado> getEstadosQMeConhecem() {
        return estadosQMeConhecem;
    }
    
    public void setEventos(ArrayList<String> arrayEventos){
        this.eventos = arrayEventos;
    }
    
    public void  setEstadosQMeConhecem(ArrayList<Estado> arrayEstados){
        this.estadosQMeConhecem = arrayEstados;
    }

    /**
     *  Adiciona um novo estado ao Array de estados que levam a um estado.
     * @param est
     */
    public void addEstadosQMeConhecem(Estado est) {
        estadosQMeConhecem.add(est);
    }

    /**
     *  Retorna se um estado foi ou não analisado.
     * @return jaAnalisado, boolean.
     */
    public boolean isJaAnalisado() {
        return jaAnalisado;
    }

    /**
     *  Altera o atributo jaAnalisado.
     * @param jaAnalisado, boolean.
     */
    public void setJaAnalisado(boolean jaAnalisado) {
        this.jaAnalisado = jaAnalisado;
    }

    /**
     *  Retorna o HashMap contendo os eventos ativos e seus respectivos
     * estados Destino.
     * @return transicoes, HashMap com Strings.
     */
    public HashMap<String, String> getTransicoes() {
        return transicoes;
    }
    
    public void setTransicoes(HashMap<String, String> transicoes) {
        this.transicoes = transicoes;
    }

    /**
     *  Retorna o Array contendo os eventos ativos.
     * @return eventos, ArrayList de Strings.
     */
    public ArrayList<String> getEventos() {
        return eventos;
    }

    /**
     *  Retorna se o estado foi acessado ou não.
     * @return acessado, boolean.
     */
    public boolean getAcessado() {
        return acessado;
    }

    /**
     *  Altera o atributo acessado.
     * @param acessado, boolean.
     */
    public void setAcessado(boolean acessado) {
        this.acessado = acessado;
    }

    /**
     *  Retorna o nome do estado.
     * @return nomeDoEstado, String.
     */
    public String getNomeDoEstado() {
        return nomeDoEstado;
    }

    /**
     *  Altera o nome do estado.
     * @param nomeDoEstado, String.
     */
    public void setNomeDoEstado(String nomeDoEstado) {
        this.nomeDoEstado = nomeDoEstado;
    }

    /**
     *  Retorna se o estado é um estado Inicial.
     * @return estadoInicial, booelan.
     */
    public boolean isEstadoInicial() {
        return estadoInicial;
    }

    /**
     *  Altera o atributo estadoInicial.
     * @param estadoInicial, boolean.
     */
    public void setEstadoInicial(boolean estadoInicial) {
        this.estadoInicial = estadoInicial;
    }

    /**
     *  Retorna se o estado é um estado Coacessível.
     * @return coAcessivel, boolean.
     */
    public boolean isCoacessivel() {
        return coAcessivel;
    }

    /**
     *  Altera o atributo coAcessivel.
     * @param analizadoCoAcessivel, boolean.
     */
    public void setCoacessivel(boolean analizadoCoAcessivel) {
        this.coAcessivel = analizadoCoAcessivel;
    }

    /**
     *  Retorna se o estado é um estado marcado.
     * @return estadoMarcado, boolean.
     */
    public boolean isEstadoMarcado() {
        return estadoMarcado;
    }

    /**
     *  Altera o atributo estadoMarcado.
     * @param estadoMarcado, boolean.
     */
    public void setEstadoMarcado(boolean estadoMarcado) {
        this.estadoMarcado = estadoMarcado;
    }

    /**
     *  Adiciona um evento e um estado destino ao HashMap transicoes e 
     * adiciona ao Array eventos um novo evento.
     * @param evento, String que indica o evento.
     * @param destino, String que indica o estado destino.
     */
    public void addTransicao(String evento, String destino) {
        eventos.add(evento);
        transicoes.put(evento, destino);
    }

    /**
     *  Verifica se o estado contém um determinado evento em 
     * seu conjunto de evento ativos. 
     * @param evento, String que indica o evento.
     * @return String retornado pelo método transicoes.get(evento).
     */
    public String temEvento(String evento) {
        if (transicoes.containsKey(evento)) {
            return transicoes.get(evento);
        } else {
            return null;
        }
    }

    /**
     *  Compara o nome de um estado com o nome de outro estado.
     * @param std, Estado a ser comparado.
     * @return boolean.
     */
    public boolean equals(Estado std) {
        return std.getNomeDoEstado().equals(this.getNomeDoEstado());
    }

    @Override
    public boolean equals(Object obj) {
        Estado other = (Estado) obj; 
        if (nomeDoEstado == null) {
            if (other.nomeDoEstado != null) {
                return false;
            }
        } else if (!nomeDoEstado.equals(other.nomeDoEstado)) {
            return false;
        }
        return true;
    }
    
    

    /**
     *  Marca os estados Coacessiveis.
     */
    public void marcaEstadosQMeConhecem() {
        this.setJaAnalisado(true);
        for (Estado std : estadosQMeConhecem) {
            std.setCoacessivel(true);
            if (!std.isJaAnalisado()) {
                std.marcaEstadosQMeConhecem();
            }
        }
    }

    @Override
    protected Estado clone(){
        Estado estado = new Estado();
        estado.setNomeDoEstado(nomeDoEstado);
        estado.setAcessado(acessado);
        estado.setCoacessivel(coAcessivel);
        estado.setEstadoInicial(estadoInicial);
        estado.setEstadoMarcado(estadoMarcado);
        estado.setJaAnalisado(jaAnalisado);
        estado.setEventos(eventos);
        estado.setEstadosQMeConhecem(estadosQMeConhecem);
        estado.setTransicoes(transicoes);
        return estado;
    }

    
    

}
