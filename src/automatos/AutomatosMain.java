/**
 * Classe AutomatosMain.
 * Arquivo Principal do Trabalho.
 * @author Hugaleno e Stéphanie
 */
package automatos;

public class AutomatosMain {

    public static void main(String[] args) {
        Home home = new Home();
        home.setVisible(true);
    }

}
